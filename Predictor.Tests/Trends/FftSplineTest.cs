﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Predictor.Tests.Trends
{
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Market.Extensions;
    using Market.Model;

    using Predictor.Trends;

    [TestClass]
    public class FftSplineTest
    {
        [TestMethod]
        public void ItHoldsAHistoryOfTheTheLastXBarGraphs()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var trend = new FftSpline();
            trend.Lookback = 100;
            trend.AddHistoricalData(bars.PriceBars);
            Assert.AreEqual(100, trend.History.Count);
        }

        [TestMethod]
        public void ItGivesInterpolatedAnswersToFutureTimes()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            var trend = new FftSpline();
            trend.Lookback = 490;
            trend.AddHistoricalData(bars.PriceBars.Take(490).ToArray());
            var interpolations = trend.Interpolate(0, 4);

            Assert.AreEqual(1256.4, interpolations[0], 3.0);
            Assert.AreEqual(1256.9, interpolations[1], 3.0);
            Assert.AreEqual(1257.3, interpolations[2], 5.0);
            Assert.AreEqual(1257.8, interpolations[3], 5.0);
        }

        [TestMethod]
        public void ItMimicsPreviousTradingCyclePertibations()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            var trend = new FftSpline();
            trend.Lookback = 490;
            trend.AddHistoricalData(bars.PriceBars.Take(490).ToArray());
            var closestCycleStart = trend.MatchCycle(10);
            Assert.AreNotEqual(0, closestCycleStart);
        }

        [TestMethod]
        public void ItIncludesTheMarketTrendInThePredictedValues()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            var trend = new FftSpline();
            trend.Lookback = 490;
            trend.AddHistoricalData(bars.PriceBars.Take(490).ToArray());
            var interpolations = trend.Interpolate(0, 4);
            var predictions = trend.Predict(4);

            Assert.AreEqual(1256.4, predictions[0], 3.0);
            Assert.AreEqual(1256.9, predictions[1], 3.0);
            Assert.AreEqual(1257.3, predictions[2], 5.0);
            Assert.AreEqual(1257.8, predictions[3], 5.0);
        }

        [TestMethod]
        public void IsShouldBeAbleToIngestData()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            var trend = new FftSpline();
            int startIndex = 300;
            trend.Lookback = startIndex;
            trend.SplineLength = 10;
            trend.AddHistoricalData(bars.PriceBars.Take(startIndex).ToArray());

            for (int i = 0; i < bars.PriceBars.Length - startIndex - 4; i++)
            {
                trend.AddHistoricalData(new [] { bars.PriceBars[startIndex + i]});

                var interpolations = trend.Interpolate(0, 4);
                var predictions = trend.Predict(4);

                Assert.AreEqual(FftSpline.TypicalPrice(bars.PriceBars[startIndex + i + 0]), predictions[0], 7.5);
                //Assert.AreEqual(FftSpline.TypicalPrice(bars.PriceBars[startIndex + i + 1]), predictions[1], 5.0);
                //Assert.AreEqual(FftSpline.TypicalPrice(bars.PriceBars[startIndex + i + 2]), predictions[2], 5.0);
                //Assert.AreEqual(FftSpline.TypicalPrice(bars.PriceBars[startIndex + i + 3]), predictions[3], 15.0);
                if (i % 100 == 0)
                {
                    Assert.AreEqual(FftSpline.TypicalPrice(bars.PriceBars[startIndex + i + 2]), predictions[2], 5.0);
                }
            }

        }

        private FftSpline CreateFftSpline(int count)
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var result = new FftSpline();
            result.Lookback = count;
            result.AddHistoricalData(bars.PriceBars);
            return result;
        }
    }
}
