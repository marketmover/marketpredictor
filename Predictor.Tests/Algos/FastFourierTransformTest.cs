﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Predictor.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Numerics;

    using Market.Extensions;
    using Market.Model;

    using MathNet.Numerics.IntegralTransforms;

    [TestClass]
    public class FastFourierTransformTest
    {
        [TestMethod]
        public void TestDoFastFourierTransform()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<Complex>(bars.PriceBars.Length);
            foreach (var data in bars.PriceBars)
            {
                samples.Add(new Complex(((double)(data.Open + data.Close + data.Low))/ 3.0, 0));
            }

            var result = Fourier.NaiveForward(samples.ToArray(), FourierOptions.Default);

            Assert.IsNotNull(result);

            var inverse = Fourier.NaiveInverse(result, FourierOptions.Default);
            Assert.IsNotNull(inverse);
        }

        /// <summary>
        /// Adding additional length does not give accurate results
        /// </summary>
        [TestMethod]
        public void TestExtendFunction()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<Complex>(bars.PriceBars.Length);
            foreach (var data in bars.PriceBars)
            {
                samples.Add(new Complex(((double)(data.Open + data.Close + data.Low)) / 3.0, 0));
            }

            var result = Fourier.NaiveForward(samples.ToArray(), FourierOptions.Default);

            var extendedSamples = new List<Complex>(result.Length + 5);
            extendedSamples.AddRange(result);
            for (int i = 0; i < 5; i++)
            {
                extendedSamples.Add(new Complex());
            }

            var inverse = Fourier.NaiveInverse(extendedSamples.ToArray(), FourierOptions.Default);
            Assert.IsNotNull(inverse);

            var frequencyScale = Fourier.FrequencyScale(500, 500);

            Assert.IsNotNull(frequencyScale);

            var largeFrequencies = new Dictionary<double, Complex>();
            for(int i = 0; i < result.Length; i++)
            {
                var frequency = result[i];
                if (frequency.Real > 10 || frequency.Imaginary > 10)
                {
                    largeFrequencies.Add(frequencyScale[i], frequency);
                }

            }
            Assert.IsTrue(largeFrequencies.Count > 2);

        }

        [TestMethod]
        public void TestCalculateValueAtInterval()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<Complex>(bars.PriceBars.Length);
            foreach (var data in bars.PriceBars)
            {
                samples.Add(new Complex(((double)(data.Open + data.Close + data.Low)) / 3.0, 0));
            }

            var result = Fourier.NaiveForward(samples.ToArray(), FourierOptions.Default);

            var frequencyScale = Fourier.FrequencyScale(500, 500);

            for (int wIndexStep = 0; wIndexStep < 500; wIndexStep++)
            {
                var first = 0.0;
                double m_N = Math.Sqrt(500);
                for (int i = 0; i < 500; i++)
                {
                    var frequency = -frequencyScale[i];
                    double wAngleInc = (frequency * 2.0 * Math.PI) * (wIndexStep / 500.0);
                    double wMulRe = Math.Cos(wAngleInc);
                    double wMulIm = Math.Sin(wAngleInc);

                    var value = result[i].Real * wMulRe + result[i].Imaginary * wMulIm;

                    first += value / m_N;
                }
                Assert.IsTrue(Math.Abs(samples[wIndexStep].Real - first) < 0.02, "Failed at index " + wIndexStep);
            }
        }

        [TestMethod]
        public void TestCalculateValueWithStrongHarmonics()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<Complex>(bars.PriceBars.Length);
            foreach (var data in bars.PriceBars)
            {
                samples.Add(new Complex(((double)(data.Open + data.Close + data.Low)) / 3.0, 0));
            }

            var result = Fourier.NaiveForward(samples.ToArray(), FourierOptions.Default);

            var frequencyScale = Fourier.FrequencyScale(500, 500);
            var largeFrequencies = new Dictionary<double, Complex>();
            for (int i = 0; i < result.Length; i++)
            {
                var frequency = result[i];
                if (Math.Abs(frequency.Real) > 10 || Math.Abs(frequency.Imaginary) > 10)
                {
                    largeFrequencies.Add(frequencyScale[i], frequency);
                }
            }


            int wIndexStep = 2;
            var first = 0.0;
            double m_N = Math.Sqrt(500);
            foreach(var kvp in largeFrequencies)
            {
                var frequency = -kvp.Key;
                double wAngleInc = (frequency * 2.0 * Math.PI) * (wIndexStep / 500.0);
                double wMulRe = Math.Cos(wAngleInc);
                double wMulIm = Math.Sin(wAngleInc);

                var value = kvp.Value.Real * wMulRe + kvp.Value.Imaginary * wMulIm;

                first += value / m_N;
            }
            Assert.IsTrue(Math.Abs(samples[wIndexStep].Real - first) < 5.00, "Failed at index " + wIndexStep);
        }
    }
}
