﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Predictor.Tests.Algos
{
    using System.Configuration;
    using System.IO;
    using System.Numerics;

    using Market.Extensions;
    using Market.Model;

    using MathNet.Numerics.Interpolation;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SplineTest
    {
        [TestMethod]
        public void TestInterpolate()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Take(490).ToArray());
            var times = new List<double>();
            for (int i = 0; i < 490; i++) times.Add(i);

            IInterpolation it = CubicSpline.InterpolateNatural(times.ToArray(), samples.ToArray());

            var prediction = it.Interpolate(491);
            Assert.AreEqual(1256.4, prediction, 2.5);
            var prediction2 = it.Interpolate(492);
            Assert.AreEqual(1256.9, prediction2, 4.0);
            var prediction3 = it.Interpolate(493);
            Assert.AreEqual(1257.3, prediction3, 8.0);
            var prediction4 = it.Interpolate(494);
            Assert.AreEqual(1257.8, prediction4, 16.0);
        }

        [TestMethod]
        public void TestCubicSplineInterpolateLast10()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Skip(480).Take(10).ToArray());
            var times = new List<double>();
            for (int i = 0; i < 10; i++) times.Add(i);

            IInterpolation it = CubicSpline.InterpolateNatural(times.ToArray(), samples.ToArray());

            var prediction = it.Interpolate(11);
            Assert.AreEqual(1256.4, prediction, 2.5);
            var prediction2 = it.Interpolate(12);
            Assert.AreEqual(1256.9, prediction2, 4.0);
            var prediction3 = it.Interpolate(13);
            Assert.AreEqual(1257.3, prediction3, 8.0);
            var prediction4 = it.Interpolate(14);
            Assert.AreEqual(1257.8, prediction4, 16.0);
        }



        [TestMethod]
        public void TestAkimaInterpolate()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Take(490).ToArray());
            var times = new List<double>();
            for (int i = 0; i < 490; i++) times.Add(i);

            IInterpolation it = CubicSpline.InterpolateAkima(times.ToArray(), samples.ToArray());

            var prediction = it.Interpolate(491);
            Assert.AreEqual(1256.4, prediction, 2.5);
            var prediction2 = it.Interpolate(492);
            Assert.AreEqual(1256.9, prediction2, 3.0);
            var prediction3 = it.Interpolate(493);
            Assert.AreEqual(1257.3, prediction3, 4.0);
            var prediction4 = it.Interpolate(494);
            Assert.AreEqual(1257.8, prediction4, 8.0);
        }

        [TestMethod]
        public void TestEquidistantPolynomialInterpolate()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Skip(486).Take(4).ToArray());

            IInterpolation it = Barycentric.InterpolatePolynomialEquidistant(1, 4, samples.ToArray());

            var prediction = it.Interpolate(5);
            Assert.AreEqual(1256.4, prediction, 2.5);
            var prediction2 = it.Interpolate(6);
            Assert.AreEqual(1256.9, prediction2, 3.0);
            var prediction3 = it.Interpolate(7);
            Assert.AreEqual(1257.3, prediction3, 4.0);
            var prediction4 = it.Interpolate(8);
            Assert.AreEqual(1257.8, prediction4, 8.0);
        }

        [TestMethod]
        public void TestNevillePolynomialInterpolation()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Skip(486).Take(4).ToArray());
            var times = new List<double>();
            for (int i = 0; i < 4; i++) times.Add(i);

            IInterpolation it = new NevillePolynomialInterpolation(times.ToArray(), samples.ToArray());

            var prediction = it.Interpolate(5);
            Assert.AreEqual(1256.4, prediction, 2.5);
            var prediction2 = it.Interpolate(6);
            Assert.AreEqual(1256.9, prediction2, 3.0);
            var prediction3 = it.Interpolate(7);
            Assert.AreEqual(1257.3, prediction3, 4.0);
            var prediction4 = it.Interpolate(8);
            Assert.AreEqual(1257.8, prediction4, 8.0);
        }
        
    }
}
