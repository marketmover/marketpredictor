﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Predictor.Tests.Algos
{
    using System.Configuration;
    using System.IO;
    using System.Numerics;

    using Market.Extensions;
    using Market.Model;

    using MathNet.Numerics.IntegralTransforms;
    using MathNet.Numerics.Interpolation;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SplineWithFft
    {
        [TestMethod]
        public void TestSplineWithStandoutDftFrequencies()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Take(490).ToArray());
            var times = new List<double>();
            for (int i = 0; i < 490; i++) times.Add(i);

            IInterpolation it = CubicSpline.InterpolateNatural(times.ToArray(), samples.ToArray());

            var dftSamples = new List<Complex>(bars.PriceBars.Length);
            foreach (var data in bars.PriceBars.Take(490))
            {
                dftSamples.Add(new Complex(((double)(data.Open + data.Close + data.Low)) / 3.0, 0));
            }

            var frequencyScale = Fourier.FrequencyScale(dftSamples.Count, dftSamples.Count);

            var dftResult = Fourier.NaiveForward(dftSamples.ToArray(), FourierOptions.Default);
            var largeFrequencies = new Dictionary<double, Complex>();
            for (int i = 0; i < dftResult.Length; i++)
            {
                var frequency = dftResult[i];
                if (Math.Abs(frequency.Real) > 10 || Math.Abs(frequency.Imaginary) > 10)
                {
                    largeFrequencies.Add(frequencyScale[i], frequency);
                }
            }

            double m_N = Math.Sqrt(dftSamples.Count);

            int startPosition = 0;// GetFrequencyAngle(largeFrequencies);
            double offset = CalculateFftOffset(largeFrequencies, startPosition + 1, m_N);
            var prediction = it.Interpolate(491);
            var updatedPrediction = prediction + offset;
            Assert.AreEqual(1256.4, updatedPrediction, 0.9);

            offset = CalculateFftOffset(largeFrequencies, startPosition + 2, m_N);
            var prediction2 = it.Interpolate(491);
            var updatedPrediction2 = prediction2 + offset;
            Assert.AreEqual(1256.9, updatedPrediction2, 1.2);
            var prediction3 = it.Interpolate(493);
            offset = CalculateFftOffset(largeFrequencies, startPosition + 3, m_N);
            var updatedPrediction3 = prediction3 + offset;
            Assert.AreEqual(1257.3, updatedPrediction3, 3.0);
            var prediction4 = it.Interpolate(494);
            offset = CalculateFftOffset(largeFrequencies, startPosition + 4, m_N);
            var updatedPrediction4 = prediction4 + offset;
            Assert.AreEqual(1257.8, updatedPrediction4, 6.0);
        }

        [TestMethod]
        public void TestSplineWithStandoutDftFrequenciesNext5()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);

            var samples = new List<double>();
            samples.AddRange(bars.PriceBars.Select(pb => (double)(pb.Open + pb.Close + pb.Low) / 3.0).Take(494).ToArray());
            var times = new List<double>();
            for (int i = 0; i < 494; i++) times.Add(i);

            IInterpolation it = CubicSpline.InterpolateNatural(times.ToArray(), samples.ToArray());

            var dftSamples = new List<Complex>(bars.PriceBars.Length);
            foreach (var data in bars.PriceBars.Take(494))
            {
                dftSamples.Add(new Complex(((double)(data.Open + data.Close + data.Low)) / 3.0, 0));
            }

            var frequencyScale = Fourier.FrequencyScale(dftSamples.Count, dftSamples.Count);

            var dftResult = Fourier.NaiveForward(dftSamples.ToArray(), FourierOptions.Default);
            var largeFrequencies = new Dictionary<double, Complex>();
            for (int i = 0; i < dftResult.Length; i++)
            {
                var frequency = dftResult[i];
                if (Math.Abs(frequency.Real) > 10 || Math.Abs(frequency.Imaginary) > 10)
                {
                    if (largeFrequencies.ContainsKey(frequencyScale[i]))
                    {
                        Assert.IsNotNull("Frequency Already added");
                    }
                    else
                    {
                        largeFrequencies.Add(frequencyScale[i], frequency);
                    }
                }
            }

            double m_N = Math.Sqrt(dftSamples.Count);

            int startAngle = GetFrequencyAngle(largeFrequencies);
            double offset = CalculateFftOffset(largeFrequencies, startAngle, m_N);
            var prediction = it.Interpolate(495);
            var updatedPrediction = prediction + offset;
            Assert.AreEqual(1258.6, updatedPrediction, 3.4);

            offset = CalculateFftOffset(largeFrequencies, startAngle + 1, m_N);
            var prediction2 = it.Interpolate(496);
            var updatedPrediction2 = prediction2 + offset;
            Assert.AreEqual(1258.1, updatedPrediction2, 2.0);
            var prediction3 = it.Interpolate(497);
            offset = CalculateFftOffset(largeFrequencies, startAngle + 2, m_N);
            var updatedPrediction3 = prediction3 + offset;
            Assert.AreEqual(1257.8, updatedPrediction3, 3.0);
            var prediction4 = it.Interpolate(498);
            offset = CalculateFftOffset(largeFrequencies, startAngle + 3, m_N);
            var updatedPrediction4 = prediction4 + offset;
            Assert.AreEqual(1258.0, updatedPrediction4, 8.0);
        }

        [TestMethod]
        public void TestCancellation()
        {
            var v1a = -1.9363194175382821;
            var v1b = 16.801746718069545;

            var v499a = -1.9363194187452477;
            var v499b = -16.801746718069545;

            var angle = 0.0;
            var result = v1a * Math.Cos(angle) + v1b * Math.Sin(angle) + v499a * Math.Cos(-angle) + v499b * Math.Sin(-angle);
            Assert.IsTrue(Math.Abs(result) > 0);
            var opposite = v1b * Math.Cos(angle) + v1a * Math.Sin(angle) + v499b * Math.Cos(-angle) + v499a * Math.Sin(-angle);
            Assert.AreEqual(0.0, opposite, 0.01);

            angle = Math.PI / 4.0;
            result = v1a * Math.Cos(angle) + v1b * Math.Sin(angle) + v499a * Math.Cos(-angle) + v499b * Math.Sin(-angle);
            Assert.IsTrue(Math.Abs(result) > 0);
            opposite = v1b * Math.Cos(angle) + v1a * Math.Sin(angle) + v499b * Math.Cos(-angle) + v499a * Math.Sin(-angle);
            Assert.AreEqual(0.0, opposite, 0.01);

            angle = Math.PI / 2.0;
            result = v1a * Math.Cos(angle) + v1b * Math.Sin(angle) + v499a * Math.Cos(-angle) + v499b * Math.Sin(-angle);
            Assert.IsTrue(Math.Abs(result) > 0);
            opposite = v1b * Math.Cos(angle) + v1a * Math.Sin(angle) + v499b * Math.Cos(-angle) + v499a * Math.Sin(-angle);
            Assert.AreEqual(0.0, opposite, 0.01);

            angle = Math.PI;
            result = v1a * Math.Cos(angle) + v1b * Math.Sin(angle) + v499a * Math.Cos(-angle) + v499b * Math.Sin(-angle);
            Assert.IsTrue(Math.Abs(result) > 0);
            opposite = v1b * Math.Cos(angle) + v1a * Math.Sin(angle) + v499b * Math.Cos(-angle) + v499a * Math.Sin(-angle);
            Assert.AreEqual(0.0, opposite, 0.01);
        }

        private static double CalculateFftOffset(Dictionary<double, Complex> largeFrequencies, int wIndexStep, double m_N)
        {
            double offset = 0.0;
            foreach (var kvp in largeFrequencies)
            {
                var frequency = -kvp.Key;
                double wAngleInc = (frequency * 2.0 * Math.PI) * (wIndexStep / 500.0);
                if (Math.Abs(wAngleInc - 0.0) > 0.01)
                {
                    double wMulRe = Math.Cos(wAngleInc);
                    double wMulIm = Math.Sin(wAngleInc);
                    var value = kvp.Value.Real * wMulRe + kvp.Value.Imaginary * wMulIm;
                    offset += value / m_N;
                }
            }
            return offset;
        }

        private static int GetFrequencyAngle(Dictionary<double, Complex> largeFrequencies)
        {
            var maxFrequency = 0.0;
            var totalFrequency = 1.0;
            foreach (var kvp in largeFrequencies)
            {
                if (kvp.Key > maxFrequency)
                {
                    maxFrequency = kvp.Key;
                }
                if (kvp.Key >= 1.0)
                {
                    totalFrequency *= Math.Abs(kvp.Key);
                }
            }
            return ((int)totalFrequency/(int)maxFrequency) % 495;
        }
    }
}
