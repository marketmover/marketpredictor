﻿(function() {

    function createCanvas(divName) {

        var div = document.getElementById(divName);
        var canvas = document.createElement('canvas');
        div.appendChild(canvas);
        if (typeof G_vmlCanvasManager != 'undefined') {
            canvas = G_vmlCanvasManager.initElement(canvas);
        }
        var ctx = canvas.getContext("2d");
        return ctx;
    }

    var dataPoints = [
        { x: new Date(2015, 01, 00) },
        { x: new Date(2015, 02, 00) },
        { x: new Date(2015, 03, 00) },
        { x: new Date(2015, 04, 00), y: [99.91, 100.15, 99.33, 99.61] },
        { x: new Date(2015, 05, 00), y: [100.12, 100.45, 99.28, 99.51] },
        { x: new Date(2015, 06, 00), y: [99.28, 100.36, 99.27, 99.79] },
        { x: new Date(2015, 07, 00), y: [99.44, 100.62, 99.41, 99.62] },
        { x: new Date(2015, 08, 00), y: [99.74, 100.45, 99.72, 99.96] },
        { x: new Date(2015, 09, 00), y: [99.31, 100.46, 98.93, 99.50] },
        { x: new Date(2015, 10, 00), y: [100.27, 100.27, 99.64, 100.19] },
        { x: new Date(2015, 11, 00), y: [100.61, 100.67, 100.05, 100.38] },
        { x: new Date(2015, 12, 00), y: [99.96, 100.11, 98.81, 99.21] },
        { x: new Date(2015, 13, 00), y: [100.40, 100.52, 99.45, 100.35] },
        { x: new Date(2015, 14, 00), y: [100.88, 100.93, 100.28, 100.65] },
        { x: new Date(2015, 15, 00), y: [100.30, 100.52, 99.76, 99.92] },
        { x: new Date(2015, 16, 00), y: [99.52, 100.29, 99.06, 99.45] },
        { x: new Date(2015, 17, 00), y: [99.25, 100.00, 99.18, 99.56] },
        { x: new Date(2015, 18, 00), y: [99.41, 100.10, 98.78, 99.67] },
        { x: new Date(2015, 19, 00), y: [100.45, 100.62, 100.19, 100.50] },
        { x: new Date(2015, 20, 00), y: [100.36, 100.54, 99.60, 100.52] },
        { x: new Date(2015, 21, 00), y: [99.52, 100.02, 99.32, 99.70] },
        { x: new Date(2015, 22, 00), y: [99.82, 100.66, 99.07, 99.73] },
        { x: new Date(2015, 23, 00), y: [100.05, 100.96, 99.51, 100.38] },
        { x: new Date(2015, 24, 00), y: [100.22, 100.66, 100.20, 100.22] },
        { x: new Date(2015, 25, 00), y: [99.05, 100.29, 98.97, 99.62] },
        { x: new Date(2015, 26, 00), y: [100.33, 100.90, 100.13, 100.78] },
        { x: new Date(2015, 27, 00), y: [100.78, 100.93, 100.75, 100.85] },
        { x: new Date(2015, 28, 00), y: [100.78, 100.92, 100.25, 100.33] },
        { x: new Date(2015, 29, 00), y: [99.75, 100.72, 99.63, 100.58] },
        { x: new Date(2015, 30, 00), y: [100.02, 100.36, 99.59, 99.85] },
        { x: new Date(2015, 31, 00), y: [100.58, 100.81, 100.56, 100.72] },
        { x: new Date(2015, 32, 00), y: [99.73, 100.08, 99.42, 99.51] },
        { x: new Date(2015, 33, 00), y: [100.16, 100.47, 99.50, 100.26] },
        { x: new Date(2015, 34, 00), y: [100.43, 100.95, 100.39, 100.88] },
        { x: new Date(2015, 35, 00) },
        { x: new Date(2015, 36, 00) },
        { x: new Date(2015, 37, 00) }
    ];

    var ctx = createCanvas("graphDiv1");

    var graph = new BarGraph(ctx);
    graph.margin = 2;
    graph.width = 500;
    graph.height = 300;

    var barCount = 12;
    var start = dataPoints.length - barCount;

    var graphIntervalId = setInterval(function() {
        start++;
        var startTextBox = document.getElementById("debug_text_box");
        if (startTextBox) {
            startTextBox.value = "i: " + start;
        }
        var array = [];
        for (var i = 0; i < barCount; i++) {
            array.push(dataPoints[(start + i) % dataPoints.length]);
        }
        graph.update(array);
    }, 1200);

    var xAxis = new AxisGrid(ctx);
    xAxis.tickMax = barCount * 2;
    xAxis.X = 3.0;
    xAxis.Y = ctx.canvas.height - xAxis.height;
    xAxis.width = 500;
    xAxis.horizontal = true;
    graph.children.push(xAxis);
    
    var candleStickSelect = document.getElementById("CandleStickGraphSelect");
    if (candleStickSelect) {
        candleStickSelect.addEventListener("change", function () {
            var marketSelect = document.getElementById("CandleStickGraphSelect");
            var bedrooms = document.getElementById("NumberOfBedroomsSelect");
            $.getJSON("/SoldPricesBy/" + marketSelect.value + "/" + bedrooms.value, function (data) {
                try {
                    clearInterval(graphIntervalId);

                    dataPoints = [
                        { x: new Date(2015, 01, 00) },
                        { x: new Date(2015, 02, 00) },
                        { x: new Date(2015, 03, 00) }
                    ];

                    if (data && data.PriceBars && data.PriceBars.length) {
                        start = Math.max(0, data.PriceBars.length - 5);

                        for (var i = 0; i < data.PriceBars.length; i++) {
                            var historicalBar = data.PriceBars[i];
                            if (historicalBar) {
                                dataPoints.push(
                                    {
                                        x: historicalBar.BarDate,
                                        y: [historicalBar.Open,
                                            historicalBar.High,
                                            historicalBar.Low,
                                            historicalBar.Close]
                                    }
                                );
                            }
                        }
                    }
                    dataPoints.push({ x: new Date(2015, 03, 00) });
                    dataPoints.push({ x: new Date(2015, 03, 00) });
                    dataPoints.push({ x: new Date(2015, 03, 00) });

                    graphIntervalId = setInterval(function () {
                        start++;
                        var startTextBox = document.getElementById("debug_text_box");
                        if (startTextBox) {
                            startTextBox.value = "i: " + start;
                        }
                        var array = [];
                        for (var i = 0; i < barCount; i++) {
                            array.push(dataPoints[(start + i) % dataPoints.length]);
                        }
                        graph.update(array);
                    }, 1200);

                } catch (e) {
                    console.error(e.toString());
                }
            });
        });
        var bedroomSelect = document.getElementById("NumberOfBedroomsSelect");
        if (bedroomSelect) {
            bedroomSelect.addEventListener("change", function () {
                var marketSelect = document.getElementById("CandleStickGraphSelect");
                var bedrooms = document.getElementById("NumberOfBedroomsSelect");
                $.getJSON("/SoldPricesBy/" + marketSelect.value + "/" + bedrooms.value, function (data) {
                    try {
                        clearInterval(graphIntervalId);

                        dataPoints = [
                            { x: new Date(2015, 01, 00) },
                            { x: new Date(2015, 02, 00) },
                            { x: new Date(2015, 03, 00) }
                        ];

                        if (data && data.PriceBars && data.PriceBars.length) {
                            start = Math.max(0, data.PriceBars.length - 5);

                            for (var i = 0; i < data.PriceBars.length; i++) {
                                var historicalBar = data.PriceBars[i];
                                if (historicalBar) {
                                    dataPoints.push(
                                        {
                                            x: historicalBar.BarDate,
                                            y: [historicalBar.Open,
                                                historicalBar.High,
                                                historicalBar.Low,
                                                historicalBar.Close]
                                        }
                                    );
                                }
                            }
                        }
                        dataPoints.push({ x: new Date(2015, 03, 00) });
                        dataPoints.push({ x: new Date(2015, 03, 00) });
                        dataPoints.push({ x: new Date(2015, 03, 00) });

                        graphIntervalId = setInterval(function () {
                            start++;
                            var startTextBox = document.getElementById("debug_text_box");
                            if (startTextBox) {
                                startTextBox.value = "i: " + start;
                            }
                            var array = [];
                            for (var i = 0; i < barCount; i++) {
                                array.push(dataPoints[(start + i) % dataPoints.length]);
                            }
                            graph.update(array);
                        }, 1200);

                    } catch (e) {
                        console.error(e.toString());
                    }
                });
            });
        }
        var event; // The custom event that will be created

        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            event.initEvent("change", true, true);
        } else {
            event = document.createEventObject();
            event.eventType = "change";
        }
        event.eventName = "change";

        if (document.createEvent) {
            candleStickSelect.dispatchEvent(event);
        } else {
            candleStickSelect.fireEvent("on" + event.eventType, event);
        }
    }
}());