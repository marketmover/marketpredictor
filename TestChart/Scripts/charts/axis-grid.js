﻿function AxisGrid(ctx) {
    var self = this;

    this.X = 0;
    this.Y = 0;
    this.horizontal = 0;
    this.height = 10;
    this.width = 100;
    this.tickColor = "#333";
    this.tickInterval = 1.0;
    this.tickMax = 20.0;
    this.tickMin = 0.0;

    this.drawHorizontalAxis = function(x, y, width, height) {
        var interval = (self.tickInterval) / (self.tickMax - self.tickMin) * width;
        var axisLineY = y + (height / 2);
        ctx.fillStyle = self.tickColor;
        ctx.fillRect(x, axisLineY, width, 2);
        var barHeight = height / 2.0;
        var barHalfHeight = barHeight / 2.0;

        for (var i = 0; i < width; i+=interval) {
            var tickX = i  + x;
            var tickY = y + barHalfHeight;
            ctx.fillRect(tickX, tickY, 1, barHeight);
        }
    };

    this.drawVerticalAxis = function(x, y, width, height) {
        //var tickSize = (self.tickMax - self.tickMin);
        //var interval = tickSize / self.tickInterval;
        //var axisLineX = x + (width / 2);
        //ctx.fillStyle = self.tickColor;
        //ctx.fillRect(axisLineX, y, 2, height);
        //var barWidth = width / 2.0;
        //var barHalfWidth = barWidth / 2.0;

        //for (var i = 0; i < interval; i++) {
        //    var tickX = x + barHalfWidth;
        //    var tickY = i * interval + y;
        //    ctx.fillRect(tickX, tickY, barWidth, 1);
        //}
    };

    this.draw = function (x, y, width, height) {
        if (self.horizontal) {
            self.drawHorizontalAxis(x, y, width, height);
        } else {
            self.drawVerticalAxis(x, y, width, height);
        }
    };
}

