﻿function BarGraph(ctx) {

    // Private properties and methods

    var self = this;

    // Draw method updates the canvas with the current display
    var draw = function(arr) {

        var numOfBars = arr.length;
        var barWidth;
        var barHeight;
        var border = 2;
        var maxBarHeight;
        var graphAreaWidth = self.width;
        var graphAreaHeight = self.height;
        var i;
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June",
            "July", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];

        ctx.save();

        //ctx.setTransform(0, -1, 0, 0, 0, 0);

        // Update the dimensions of the canvas only if they have changed
        if (ctx.canvas.width !== self.width || ctx.canvas.height !== self.height) {
            ctx.canvas.width = self.width;
            ctx.canvas.height = self.height;
        }

        // Draw the background color
        ctx.fillStyle = self.backgroundColor;
        ctx.fillRect(0, 0, self.width, self.height);

        // Calculate dimensions of the bar
        barWidth = graphAreaWidth / (numOfBars) - self.margin * 2;
        maxBarHeight = graphAreaHeight - 55;

        // Determine the largest value in the bar array
        var largestValue = 0.0;
        var smallestValue = 1000000.0;

        for (i = 0; i < arr.length; i += 1) {
            if (arr[i].y && arr[i].y[1] > largestValue) {
                largestValue = arr[i].y[1];
            }
            if (arr[i].y && arr[i].y[2] < smallestValue) {
                smallestValue = arr[i].y[2];
            }
        }

        var buffer = (largestValue - smallestValue) / 4.0;
        var startHeight = smallestValue - buffer;
        var maxHeight = (largestValue - smallestValue) + buffer;

        var previousMonth = 0;
        var previousYear = 0;
        // For each bar
        for (i = 0; i < arr.length; i += 1) {
            var prices = arr[i].y;
            if (prices) {
                var open = arr[i].y[0];
                var high = arr[i].y[1];
                var low = arr[i].y[2];
                var close = arr[i].y[3];

                var candleStickBottom = graphAreaHeight - ((open - startHeight) / maxHeight) * maxBarHeight;
                var candleStickTop = graphAreaHeight - ((close - startHeight) / maxHeight) * maxBarHeight;

                var candleWickBottom = graphAreaHeight - ((low - startHeight) / maxHeight) * maxBarHeight;
                var candleWickTop = graphAreaHeight - ((high - startHeight) / maxHeight) * maxBarHeight;

                ctx.fillStyle = "#333";
                ctx.fillRect(i * self.width / numOfBars + border + barWidth / 2,
                    candleWickBottom,
                    2,
                    candleWickTop - candleWickBottom);

                if (open == close) {
                    ctx.fillRect(self.margin + i * self.width / numOfBars,
                        candleStickBottom,
                        barWidth,
                        1);
                }

                // Turn on shadow
                ctx.shadowOffsetX = 2;
                ctx.shadowOffsetY = 2;
                ctx.shadowBlur = 2;
                ctx.shadowColor = "#999";

                barHeight = (candleStickTop - candleStickBottom);
                // Draw bar background
                ctx.fillStyle = "#333";
                ctx.fillRect(self.margin + i * self.width / numOfBars,
                    candleStickBottom,
                    barWidth,
                    barHeight);

                // Turn off shadow
                ctx.shadowOffsetX = 0;
                ctx.shadowOffsetY = 0;
                ctx.shadowBlur = 0;

                if (Math.abs(barHeight) > border * 2) {
                    var offset = -border;
                    if (candleStickBottom > candleStickTop) {
                        ctx.fillStyle = "#0A0";
                    } else {
                        ctx.fillStyle = "#A00";
                        offset = border;
                    }
                    ctx.fillRect(self.margin + i * self.width / numOfBars + border,
                        candleStickBottom + offset,
                        barWidth - border * 2,
                        barHeight - offset * 2);
                }

                ctx.fillStyle = "#333";
                ctx.font = "bold 12px sans-serif";
                ctx.textAlign = "center";
                // Use try / catch to stop IE 8 from going to error town
                try {
                    ctx.fillText(self.getDisplayPrice(close),
                        i * self.width / numOfBars + (self.width / numOfBars) / 2,
                        candleWickTop - 10);

                    var barDate = new Date(arr[i].x);
                    if (barDate) {
                        var month = barDate.getMonth();
                        if (month != previousMonth) {
                            ctx.fillText(monthNames[barDate.getMonth()],
                                i * self.width / numOfBars + (self.width / numOfBars) / 2,
                                graphAreaHeight - 32);
                            if (barDate.getFullYear() != previousYear) {
                                ctx.fillText("" + (barDate.getFullYear()),
                                    i * self.width / numOfBars + (self.width / numOfBars) / 2,
                                    graphAreaHeight - 16);
                                previousYear = barDate.getFullYear();
                            }
                            previousMonth = month;
                        }
                    }
                } catch (ex) {
                }
            }
        }

        try {
            for (var index = 0; index < self.children.length; index++) {
                var child = self.children[index];
                if (child) {
                    child.draw(child.X, child.Y, child.width, child.height);
                }
            }
        } catch(ex) {
        }

        ctx.restore();
    };

    // Public properties and methods
    this.width = 300;
    this.height = 150;
    this.margin = 5;
    this.curArr = [];
    this.backgroundColor = "#fff";
    this.children = [];

    this.getDisplayPrice = function (price) {
        var displayPrice = parseFloat(price);
        
        var unit = "";
        if (displayPrice >= 1000000) {
            unit = "M";
            displayPrice = displayPrice / 1000000.0;
        }
        else if (price >= 1000) {
            unit = "k";
            displayPrice = displayPrice / 1000.0;
        }
        if (displayPrice >= 100) {
            displayPrice = Math.round(displayPrice * 100) / 100;
        }
        else if (displayPrice >= 10) {
            displayPrice = Math.round(displayPrice * 10) / 10;
        }

        return Math.round(parseFloat(displayPrice, 10) * 1000) / 1000 + unit;
    };

    this.update = function (newArr) {

        self.curArr = newArr;
        draw(newArr);
    };
}