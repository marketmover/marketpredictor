﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestChart.Controllers
{
    using System.IO;
    using System.Web;

    using Market.Extensions;
    using Market.Model;

    public class SoldPricesController : ApiController
    {
        // GET api/soldprices
        public string[] Get()
        {
            return new [] {"AUD", "Minute", "Richmond"};
        }

        // GET api/soldprices/suburb
        public HistoricalBars GetCharts(string suburb, string bedrooms)
        {
            if (suburb == "AUD")
            {
                var filename = HttpContext.Current.Server.MapPath("/Scripts/charts/data/AUDWeeklyData.json");
                var prices = File.ReadAllText(filename);
                var priceBars = prices.FromJson<HistoricalBars>();
                return priceBars;

            }

            if (suburb == "Minute")
            {
                var filename = HttpContext.Current.Server.MapPath("/Scripts/charts/data/MinuteBarData.json");
                var prices = File.ReadAllText(filename);
                var priceBars = prices.FromJson<HistoricalBars>();
                return priceBars;
            }

            if (suburb == "Richmond")
            {
                return new HistoricalBars();
            }

            throw new HttpException(404, suburb + " not available");
        }

        // POST api/soldprices
        public void Post([FromBody]string value)
        {
        }

        // PUT api/soldprices/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/soldprices/5
        public void Delete(int id)
        {
        }
    }
}
