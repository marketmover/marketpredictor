﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TestChart
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "SoldPrices",
                routeTemplate: "SoldPricesBy/{suburb}/{bedrooms}",
                defaults: new { controller = "SoldPrices", action = "GetCharts", suburb = "AUD", bedrooms = "2" }
);

        }
    }
}
