﻿namespace Predictor.Trends
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Numerics;

    using Market.Model;

    using MathNet.Numerics.IntegralTransforms;
    using MathNet.Numerics.Interpolation;

    public class FftSpline
    {
        public FftSpline()
        {
            History = new List<CandleStick>();
            SplineLength = 10;
            Lookback = 100;
            MovingAverageLength = 14;
        }

        public int Lookback { get; set; }
        public int SplineLength { get; set; }
        public List<CandleStick> History { get; set; }
        public int MovingAverageLength { get; set; }

        public void AddHistoricalData(CandleStick[] priceBars)
        {
            History.AddRange(priceBars);
            if (History.Count > Lookback)
            {
                History.RemoveRange(0, History.Count - Lookback);
            }
        }

        public double[] Interpolate(int knownData, int predictedData)
        {
            var samples = new List<double>();
            samples.AddRange(History.Select(TypicalPrice).Skip(Lookback-SplineLength).ToArray());
            var times = new List<double>();
            for (int i = 0; i < samples.Count; i++) times.Add(i);
            //times.AddRange(History.Select(pb => (double)pb.BarDate.Ticks));

            IInterpolation it = CubicSpline.InterpolateNatural(times.ToArray(), samples.ToArray());
            var result = new List<double>();
            for (int i = 0; i < knownData; i++)
            {
                result.Add(it.Interpolate(samples.Count - (knownData+ i)));
            }
            for (int i = 0; i < predictedData; i++)
            {
                result.Add(it.Interpolate(samples.Count + i));
            }
            return result.ToArray();
        }

        public double[] Predict(int count)
        {
            var result = new List<double>();
            var startCycle = this.MatchCycle(10);
            var largeFrequencies = this.FindLargeFrequencies();
            var pertibations = this.GeneratePertibations(largeFrequencies, startCycle, count);
            var interpolations = this.Interpolate(0, count);
            for (int i = 0; i < count; i++)
            {
                result.Add(interpolations[i] + pertibations[i]);
            }
            return result.ToArray();
        }

        public static double TypicalPrice(CandleStick pb)
        {
            return (double)(pb.Open + pb.Close + pb.Low) / 3.0;
        }

        private static Complex ComplexTypicalPrice(CandleStick pb)
        {
            return new Complex((double)(pb.Open + pb.Close + pb.Low) / 3.0, 0.0);
        }

        public int MatchCycle(int samples)
        {
            var largeFrequencies = this.FindLargeFrequencies();

            var previousValue = GetMovingAverage(History.Count - samples);

            var signature = new List<double>();
            for (int i = samples; i > 0; i--)
            {
                var index = History.Count - i;
                var movingAverage = GetExponetialMovingAverage(index, previousValue);
                var difference = TypicalPrice(History[index]) - movingAverage;
                signature.Add(difference);
            }
            var startIndex = MatchPattern(largeFrequencies, signature);
            return startIndex + samples;
        }

        private Dictionary<double, Complex> FindLargeFrequencies()
        {
            var largeFrequencies = new Dictionary<double, Complex>();

            var frequencyScale = Fourier.FrequencyScale(this.History.Count, this.History.Count);
            var dftResult = Fourier.NaiveForward(this.History.Select(ComplexTypicalPrice).ToArray(), FourierOptions.Default);
            for (int i = 0; i < dftResult.Length; i++)
            {
                var frequency = dftResult[i];
                if (Math.Abs(frequency.Real) > 10 || Math.Abs(frequency.Imaginary) > 10)
                {
                    largeFrequencies.Add(frequencyScale[i], frequency);
                }
            }
            return largeFrequencies;
        }

        private int MatchPattern(Dictionary<double, Complex> strongFrequencies,List<double> signature)
        {
            var pertibations = GeneratePertibations(strongFrequencies, 0, History.Count);

            var minDifference = double.MaxValue;
            var result = 0;
            var length = History.Count - signature.Count * 2;
            for (int i = 0; i < length; i++)
            {
                var difference = CalculateDifference(pertibations, signature, i);
                if (Math.Abs(difference) < minDifference)
                {
                    result = i;
                    minDifference = Math.Abs(difference);
                }
            }

            return result;
        }

        private double CalculateDifference(double[] pertibations, List<double> signature, int startIndex)
        {
            var result = 0.0;
            for (int i = 0; i < signature.Count; i++)
            {
                result += pertibations[startIndex + i] - signature[i];
            }
            return result;
        }

        public double[] GeneratePertibations(Dictionary<double, Complex> strongFrequencies, int startFrequency, int count)
        {
            var result = new List<double>();
            double factor = Math.Sqrt(History.Count);
            for (int i = startFrequency; i < startFrequency + count; i++)
            {
                result.Add(CalculateFft(strongFrequencies, startFrequency + i, factor));
            }
            return result.ToArray();
        }

        private static double CalculateFft(Dictionary<double, Complex> largeFrequencies, int wIndexStep, double m_N)
        {
            double offset = 0.0;
            foreach (var kvp in largeFrequencies)
            {
                var frequency = -kvp.Key;
                double wAngleInc = (frequency * 2.0 * Math.PI) * (wIndexStep / 500.0);
                if (Math.Abs(wAngleInc - 0.0) > 0.01)
                {
                    double wMulRe = Math.Cos(wAngleInc);
                    double wMulIm = Math.Sin(wAngleInc);
                    var value = kvp.Value.Real * wMulRe + kvp.Value.Imaginary * wMulIm;
                    offset += value / m_N;
                }
            }
            return offset;
        }

        private double GetExponetialMovingAverage(int index, double previousValue)
        {
            var result = previousValue * (2 / (13)) + TypicalPrice(History[index]) * (1 - 2 / (13));
            return result;
        }

        private double GetMovingAverage(int index)
        {
            double result = 0.0;
            for (int i = index - MovingAverageLength + 1; i <= index; i++)
            {
                if (i < 0 || i > History.Count)
                {
                    System.Diagnostics.Debug.WriteLine("Array Out of bounds");
                }
                result += TypicalPrice(History[i]);
            }
            return result / MovingAverageLength;
        }

    }
}
