# Market Mover

## Purpose

The market mover uses past performance of a market to predict the bounds a market would be expected to move.


## Method
### DFT available data points
Perform DFT on available data points to derive time scaled sine functions of market behavior.

### Indicator Predictors

Train random predictors based upon the DFT functions and available trading indicators using Haar Cascades and AdaBoost methods for each selected market.

Continually train predictors with incomming data and update predictions, discarding predictors that fail to correctly identify market direction better than a newly trained predictor.

Test if any predictor perform well on related markets.

### Incorporate Delta Factors 
Implement a method of incorporating the effects on the general market direction due to a press release or external event. Available feeds allow for a low, med, high effect and previous, predicted and new value.  These points provide pivot points on the overall market trend and can be used to adjust the predictors levels to move quickly respond to expected market fluctuations.  Need to handle large changes in prices from market close to next market open and need ability to add manual event not from covered in the feed.

### Related Markets and market sectors
Train Multi market predictors to use the aggregator predictor for a market sector and determine the related market affect and use to modify the predicted trend.

### Market Sectors
Current Market Sector Groups
* Semiconductor
* Technology
* Renewable Energies
* Fossil Fuels
* Currencies
* Metals

### Continual Training Process

```
Load Existing Predictor Definition

    Request all available dataset intervals or market and persist for future use. 
    Need to investigate a method of using existing data for testing and live incomming data for testing the predictor or break retrieved data into 3 sections and use the middle section as the test data.

    Perform DFT on previous market data to get functions for 1, 2, 3, 5, 15, 30 minutes, 1, 2, 4, 8, hours, and daily, weekly, monthly, yearly.
    
    Test Predictions on current data
        Remove any poorly performing predictors (< 80% accuracy)

Create Basic Predictors
    
    Repeat until 11 predictors for each interval exist
        Randomly select an existing trading engine and randomize the indicator(s) parameters
        
        Test Predictors over available data sets for market at the same interval on ability to predict the correct market direction (up or down) at 1, 2, 3, and 4 based upon the sine wave function passed in.
        
        If the result of simulation at 1, 2, 3, and 4 was > 51% keep, otherwise throw away at start again
        
        For both the up and down nodes, randomly select an existing trading engine and randomize the indicator parameters
        
            Test the predictor by starting at the top node sending the data through the predictor tree.  Discard the predictor if it was not accurate more than 51% of the time based upon the number of samples the node under training saw or less than X samples were scene (X ~= 5). If less than X samples were scene, discard node and end.
            
            If more than X samples were scene by the node, repeat training on up and down nodes.
    
    Keep the 10 best performing predictors and discard the worse one at each interval
    
    Assign weights for predictors to be the normalized percentage of its prediction quality.
    
    If training data available
    Use AdaBoost to fine tune the aggregate predictor for X (X ~=100/20/5) interations
        
        modify each of the predictor weights by and 10 - random(20)
        
        Train over selected training data
        
        Verify over testing data and keep weights if predictor performs better than original values.
        
        Repeat by modifying the predictor weights by 1 - random(2) and another loop at 0.1 - random(0.2)
``` 

### Improved Training
Use the press release events market trend to adjust the market price before hitting the predictors. Forcast market trends based upon available press event real value if the event is in the past and the predicted value if available and strength of the press release.

### Minimum volitility
The market must move more than the spread for it to be a viable market to trade in.  If the market movement in the past X intervals (X ~= 20) < 4 * spread, only send market exit signals.

## Monetization

### Single Market Data Notifications
To target the profession trader or day trader and provide a service that will send a Push updates to a mobile app to notify the interested party of the stocks being watched and the current predictions

The information provided would show the current market status and the automatically trained predictors expected high, low, and close for the next 4 intervals.

Charge a daily/weekly/monthly fee to access the feed per stock.

### Prediction Buy and Sell signals for the watched market
Allow users to tune a prediction engine on a market to receive the buy and sell signals at a small cost/hour. Integrates into the tradefair development environment.

SMS notification if service is down.

### HA/Scalability 
System uses highly available and horizontally scalable infrastructure to train markets predictors and delivery the content to the masses. Web site has cached data up to 5 minutes old to encourage the "premium" service.  
