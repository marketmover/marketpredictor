﻿namespace Market.Model
{
    public class HistoricalBars
    {
        public CandleStick[] PriceBars { get; set; }
        public CandleStick PartialPriceBar { get; set; }
    }
}
