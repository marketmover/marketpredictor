﻿namespace Market.Model
{
    public class SessionResponse
    {
        public bool AllowedAccountOperator { get; set; }
        public bool PasswordChangeRequired { get; set; }
        public string Session { get; set; }
    }
}
