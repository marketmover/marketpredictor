﻿namespace Market.Model
{
    public class SessionRequest
    {
        public string AppComments { get; set; }
        public string AppKey { get; set; }
        public string AppVersion { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
