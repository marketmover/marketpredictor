﻿using System;

namespace Market.Model
{
    public class CandleStick
    {
        public DateTime BarDate { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
    }
}
