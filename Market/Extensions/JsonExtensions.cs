﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Market.Extensions
{

        public static class JsonExtensions
        {
            public static string ToJson<T>(this T parent)
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                using (var tempStream = new MemoryStream())
                {
                    serializer.WriteObject(tempStream, parent);
                    return Encoding.Default.GetString(tempStream.ToArray());
                }
            }

            public static T FromJson<T>(this string json)
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                using (var tempStream = new MemoryStream(Encoding.Unicode.GetBytes(json)))
                {
                    return (T)serializer.ReadObject(tempStream);
                }
            }

            private const string INDENT_STRING = "    ";
            public static string ToFormattedJson<T>(this T parent)
            {
                var indent = 0;
                var quoted = false;
                var sb = new StringBuilder();
                var str = parent.ToJson();
                for (var i = 0; i < str.Length; i++)
                {
                    var ch = str[i];
                    switch (ch)
                    {
                        case '{':
                        case '[':
                            sb.Append(ch);
                            if (!quoted)
                            {
                                sb.AppendLine();
                                Enumerable.Range(0, ++indent).ForEach(item => sb.Append(INDENT_STRING));
                            }
                            break;
                        case '}':
                        case ']':
                            if (!quoted)
                            {
                                sb.AppendLine();
                                Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
                            }
                            sb.Append(ch);
                            break;
                        case '"':
                            sb.Append(ch);
                            bool escaped = false;
                            var index = i;
                            while (index > 0 && str[--index] == '\\')
                                escaped = !escaped;
                            if (!escaped)
                                quoted = !quoted;
                            break;
                        case ',':
                            sb.Append(ch);
                            if (!quoted)
                            {
                                sb.AppendLine();
                                Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
                            }
                            break;
                        case ':':
                            sb.Append(ch);
                            if (!quoted)
                                sb.Append(" ");
                            break;
                        default:
                            sb.Append(ch);
                            break;
                    }
                }
                return sb.ToString();
            }

            public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
            {
                foreach (var i in ie)
                {
                    action(i);
                }
            }
        }
}
