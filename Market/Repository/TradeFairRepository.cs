﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Repository
{
    using System.Configuration;
    using System.Net;

    using Market.Extensions;
    using Market.Model;

    using RestSharp;

    public class TradeFairRepository
    {
        public TradeFairRepository(string username, string password)
        {
            UserName = username;
            Password = password;
        }

        private string UserName { get; set; }
        private string Password { get; set; }
        private string Session { get; set; }

        public bool CreateSession()
        {
            var client = new RestClient(ConfigurationManager.AppSettings["TradeFairUrl"]);
            var request = new RestRequest("/TradingApi/session");
            request.Method = Method.POST;
            request.AddHeader("content-type", "application/json; charset=utf-8");
            var osVersion = Environment.OSVersion.ToString();
            var sessionRequest = new SessionRequest
                                     {
                                         AppComments = osVersion,
                                         AppKey = "ATC-TradeFair",
                                         AppVersion = "327",
                                         Password = Password,
                                         UserName = UserName
                                     };

            request.AddJsonBody(sessionRequest);
            var response = client.Execute(request);
            var sessionResponse = response.Content.FromJson<SessionResponse>();
            Session = sessionResponse.Session;
            return Session != null;
        }

        public bool DeleteSession()
        {
            var client = new RestClient(ConfigurationManager.AppSettings["TradeFairUrl"]);
            var request =
                new RestRequest(
                    string.Format("/TradingApi/session/deleteSession/userName={0}&session={1}", UserName, Session));
            var response = client.Execute(request);
            return response.Content.Contains("LoggedOut\":true");
        }

        public HistoricalBars BarHistory(string market, string interval, int span, int priceBars, string priceType)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["TradeFairUrl"]);
            var request =
                new RestRequest(
                    string.Format(
                        "/TradingApi/market/{0}/barhistory?interval={1}&span={2}&pricebars={3}&PriceType={4}",
                        market,
                        interval,
                        span,
                        priceBars,
                        priceType));
            request.AddHeader("Session", Session);
            request.AddHeader("UserName", UserName);
            var result = client.Execute(request);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                return result.Content.FromJson<HistoricalBars>();
            }
            return null;
        }
    }
}
