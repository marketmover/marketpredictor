﻿namespace Market.Tests
{
    using System.Configuration;
    using System.IO;

    using Market.Model;
    using Market.Repository;
    using Market.Extensions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class GetMinuteBarsTest
    {
        [TestMethod]
        public void TestGetMinuteData()
        {
            var userName = ConfigurationManager.AppSettings["TradeFairUserName"];
            var password = ConfigurationManager.AppSettings["TradeFairPassword"];
            var repo = new TradeFairRepository(userName, password);
            Assert.IsTrue(repo.CreateSession());
            string market = "400616153";
            string interval = "WEEK";
            int span = 1;
            int priceBars = 500;
            string priceType = "MID";

            var minuteBars = repo.BarHistory(market, interval, span, priceBars, priceType);

            Assert.AreEqual(500, minuteBars.PriceBars.Length);

            File.WriteAllText("E:\\code\\Spikes\\MarketMover\\Market.Tests\\TestData\\AUDWeeklyData.json", minuteBars.ToFormattedJson());
            repo.DeleteSession();
        }

        [TestMethod]
        public void TestParseJsonString()
        {
            var path = ConfigurationManager.AppSettings["TestDataPath"];
            var filename = Path.Combine(path, @"MinuteBarData.json");

            var jsonData = File.ReadAllText(filename);
            var bars = jsonData.FromJson<HistoricalBars>();
            Assert.IsNotNull(bars);
        }
    }
}
