﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Tests
{
    using System.Configuration;

    using Market.Model;
    using Market.Repository;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CreateSessionTest
    {
        [TestMethod]
        public void TestCreateNewSession()
        {
            var userName = ConfigurationManager.AppSettings["TradeFairUserName"];
            var password = ConfigurationManager.AppSettings["TradeFairPassword"];
            var repo = new TradeFairRepository(userName, password);
            var response = repo.CreateSession();
            Assert.IsTrue(response);
            repo.DeleteSession();
        }
    }
}
